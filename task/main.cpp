#include <fstream>
#include <iostream>
#include <string>

#include "tlv.h"

int main(int argc, char **argv) {

    if (argc != 2) {
        std::cerr << "missing argument" << std::endl;
        return EXIT_FAILURE;
    }

    std::ifstream f(argv[1], f.in | f.binary);
    if (f.fail()) {
        std::cerr << "file " << argv[1] << " could not be opened" << std::endl;
        return EXIT_FAILURE;
    }
    std::string content(std::istreambuf_iterator<char>(f), std::istreambuf_iterator<char>());

    // ...

    return EXIT_SUCCESS;
}

